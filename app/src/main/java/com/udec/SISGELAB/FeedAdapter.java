package com.udec.SISGELAB;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedViewHolder> {
    ArrayList<FeedData> arrayList = new ArrayList<>();

    public FeedAdapter(ArrayList<FeedData> arrayList, MyAdapterListener listener) {
        this.arrayList = arrayList;
        onClickListener = listener;
    }
    public MyAdapterListener onClickListener;

    public interface MyAdapterListener {

        void buttonViewOnClick(View v, int position);

    }

    @NonNull
    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new FeedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder holder, int position) {
        FeedData data = arrayList.get(position);


        Log.d("title",data.getCodigoAnalisis());
        Log.d("state",data.getEstadoAnalisis());
        holder.titleAnalisis.setText(data.getNombreAnalisis());
        holder.code.setText(data.getCodigoAnalisis());
        holder.state.setText(data.getEstadoAnalisis());
        holder.button.setTag(data.getCodigoAnalisis());
        Log.d("tag button: ", String.format("%s", holder.button.getTag()));
        if(holder.state.getText().equals("en proceso")){
            holder.state.setBackgroundResource(R.drawable.background_estado_analisis_proceso);
            holder.state.setText("En proceso");

        }else if(holder.state.getText().equals("no iniciado")){
            holder.state.setBackgroundResource(R.drawable.background_estado_analisis_no_iniciado);
            holder.state.setText("No iniciado");

        }else if(holder.state.getText().equals("en revisión")){
            holder.state.setBackgroundResource(R.drawable.background_estado_analisis_en_revision);
            holder.state.setText("En Revisión");

        }else if(holder.state.getText().equals("finalizado")){
            holder.state.setBackgroundResource(R.drawable.background_estado_analisis_finalizado);
            holder.state.setText("Finalizado");

        }else if(holder.state.getText().equals("vencido")){
            holder.state.setBackgroundResource(R.drawable.background_estado_analisis_en_vencido);
            holder.state.setText("Vencido");

        }

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.buttonViewOnClick(v, holder.getAdapterPosition());

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class FeedViewHolder extends RecyclerView.ViewHolder {
        TextView titleAnalisis, code, state;
        Button button;

        FeedViewHolder(@NonNull View itemView) {
            super(itemView);
            titleAnalisis = itemView.findViewById(R.id.titleAnalisis);
            code = itemView.findViewById(R.id.code);
            state = itemView.findViewById(R.id.titleEstadoAnalisis);
            button = itemView.findViewById(R.id.buttonDetalleAnalisis);
        }
    }


}