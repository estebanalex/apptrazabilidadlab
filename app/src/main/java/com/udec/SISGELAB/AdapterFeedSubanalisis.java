package com.udec.SISGELAB;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;

public class AdapterFeedSubanalisis extends RecyclerView.Adapter<AdapterFeedSubanalisis.FeedViewHolder> {
    ArrayList<AdapterDataSubanalisis> arrayList = new ArrayList<>();

    public AdapterFeedSubanalisis(ArrayList<AdapterDataSubanalisis> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_analisis, parent, false);
        return new FeedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder holder, int position) {
        AdapterDataSubanalisis data = arrayList.get(position);


        holder.codeSubanalisis.setText(data.getCodigoSubnalisis());
        holder.codeSubmuestra.setText(data.getCodigoSubmuestra());
        holder.nombreSubmuestra.setText(data.getNombreSubmuestra());
        holder.responsable.setText(data.getResponsable());
        holder.estado.setText(data.getEstado());
        if(holder.estado.getText().equals("en proceso")){
            holder.estado.setBackgroundResource(R.drawable.background_estado_analisis_proceso);
            holder.estado.setText("En proceso");

        }else if(holder.estado.getText().equals("no iniciado")){
            holder.estado.setBackgroundResource(R.drawable.background_estado_analisis_no_iniciado);
            holder.estado.setText("No iniciado");

        }else if(holder.estado.getText().equals("en revision")){
            holder.estado.setBackgroundResource(R.drawable.background_estado_analisis_en_revision);
            holder.estado.setText("En revisión");

        }else if(holder.estado.getText().equals("finalizado")){
            holder.estado.setBackgroundResource(R.drawable.background_estado_analisis_finalizado);
            holder.estado.setText("Finalizado");

        }else if(holder.estado.getText().equals("vencido")){
            holder.estado.setBackgroundResource(R.drawable.background_estado_analisis_en_vencido);
            holder.estado.setText("Vencido");

        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class FeedViewHolder extends RecyclerView.ViewHolder {
        TextView codeSubanalisis, codeSubmuestra, nombreSubmuestra, responsable, estado;

        FeedViewHolder(@NonNull View itemView) {
            super(itemView);
            codeSubanalisis = itemView.findViewById(R.id.codigoSubanalisis);
            codeSubmuestra = itemView.findViewById(R.id.codigoSubmuestra);
            nombreSubmuestra = itemView.findViewById(R.id.tipoSubmuestra);
            responsable = itemView.findViewById(R.id.responsable);
            estado = itemView.findViewById(R.id.titleEstadoSubanalisis);
        }
    }


}