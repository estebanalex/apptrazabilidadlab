package com.udec.SISGELAB;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdapterFeedErrorSubanalisis extends RecyclerView.Adapter<AdapterFeedErrorSubanalisis.FeedViewHolder> {
    ArrayList<AdapterDataErrorSubanalisis> arrayList = new ArrayList<>();

    public AdapterFeedErrorSubanalisis(ArrayList<AdapterDataErrorSubanalisis> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_error, parent, false);
        return new FeedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder holder, int position) {
        AdapterDataErrorSubanalisis data = arrayList.get(position);

        holder.error.setText(data.getError());


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class FeedViewHolder extends RecyclerView.ViewHolder {
        TextView error;

        FeedViewHolder(@NonNull View itemView) {
            super(itemView);
            error = itemView.findViewById(R.id.errorText);
        }
    }


}