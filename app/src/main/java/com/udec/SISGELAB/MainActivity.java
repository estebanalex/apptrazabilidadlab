package com.udec.SISGELAB;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {
    ImageView scannerView;
    String ResultadoEscaneo;
    String ip="http://192.168.43.113";
    String endpointDetalleMeustra="/api/muestras/obtener-detalle-muestra?codigo=";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        scannerView = findViewById(R.id.scannerView);
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scannerView.setEnabled(false);
                IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
                integrator.setPrompt("Escanea el codigo QR de la muestra");
                integrator.setCameraId(0);  // Use a specific camera of the device
                integrator.setBeepEnabled(true);
                integrator.setBarcodeImageEnabled(true);
                integrator.setOrientationLocked(false);
                integrator.setTimeout(10000);
                integrator.initiateScan();



                // Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //  .setAction("Action", null).show();
            }

        });


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        scannerView.setEnabled(true);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Log.d("ScannerActivity", "Cancelled scan");
                Toast.makeText(this, "Escaneo Cancelado", Toast.LENGTH_LONG).show();
            } else {

                Log.d("ScannerActivity", "Scanned");
                Log.d("RESULTADO", result.toString());
                ResultadoEscaneo =  result.getContents();
                jsonrequest();

            }

        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void jsonrequest() {
        String JSON_URL = ip+endpointDetalleMeustra+ResultadoEscaneo;
        Log.d("URL",JSON_URL);
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                    new Response.Listener<JSONObject>() {
                        @Override

                        public void onResponse(JSONObject response) {
                            // Do something with response
                            //mTextView.setText(response.toString());
                            Log.d("response", response.toString());
                            // Process the JSON


                                try{
                                    // Get the JSON array
                                    JSONArray array = response.getJSONArray("muestra");
                                    // Loop through the array elements
                                    if(array.length()==0) {
                                        JSONObject objetoError = response;
                                        Toast.makeText(getApplicationContext(), "Error: "+objetoError.getString("mensaje_resultado_contenido"), Toast.LENGTH_SHORT).show();
                                    }else{
                                        for(int i=0;i<array.length();i++){
                                            // Get current json object
                                            JSONObject muestra = array.getJSONObject(i);
                                            Log.d("Respuesta", muestra.toString());

                                            // Get the current student (json object) data

                                            String codigoMuestra = muestra.getString("codigo_muestra") ;
                                            String tipoAnimal = muestra.getString("nombre_animal");
                                            String fechaIngreso = muestra.getString("fecha_ingreso");
                                            String fechaVencimiento = muestra.getString("fecha_vencimiento");
                                            String estadoMuestra = muestra.getString("estado");
                                            // Display the formatted json data in text view

                                            Log.d("Paso","si paso por aqui");

                                            Intent intent = new Intent(MainActivity.this, DetalleMuestra.class);
                                            intent.putExtra("codigo",codigoMuestra);
                                            intent.putExtra("fecha", fechaIngreso);
                                            intent.putExtra("fechaV", fechaVencimiento);
                                            intent.putExtra("animal", tipoAnimal);
                                            intent.putExtra("estado",estadoMuestra);


                                            startActivity(intent);
                                        }

                                    }

                                }catch (JSONException e){
                                    Toast.makeText(getApplicationContext(), "Ocurrio un Error:: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }

                    },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", error.toString());
                        Toast.makeText(getApplicationContext(), " Error: "+error.toString(), Toast.LENGTH_LONG).show();

                    }
                }

        );

            requestQueue.add(jsonObjectRequest);
        }
        // Add JsonObjectRequest to the RequestQueue
}




