package com.udec.SISGELAB;

public class AdapterDataErrorSubanalisis {

    private String error;

    public AdapterDataErrorSubanalisis(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
