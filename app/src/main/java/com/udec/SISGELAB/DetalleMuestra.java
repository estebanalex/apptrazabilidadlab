package com.udec.SISGELAB;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class DetalleMuestra extends AppCompatActivity {

    TextView codigoMuestra;
    TextView fechaIngreso;
    TextView fechaVencimiento;
    TextView estadoMuestra;
    TextView tipoAnimal;
    String ip="http://192.168.43.113";
    String endpointObtenerAnalisis="/api/analisis/obtener-analisis-solicitados?codigo=";

    String endpointObtenerDetalleAnalisis="/api/analisis/obtener-detalle-analisis?codigo=";

    ViewGroup recyLa;
    RecyclerView recyclerView;
    FeedAdapter adapter;

    ArrayList<FeedData> arrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_muestra);
        codigoMuestra = findViewById(R.id.codigoMuestra);
        fechaIngreso = findViewById(R.id.fechaIngreso);
        fechaVencimiento = findViewById(R.id.fechaVencimiento);
        estadoMuestra = findViewById(R.id.estadoMuestraText);
        tipoAnimal = findViewById(R.id.tipoAnimal);
        recyclerView = findViewById(R.id.listaAnalisis);

        recyLa = findViewById(R.id.listaAnalisisLa);
        recyclerView.setLayoutManager(new LinearLayoutManager(DetalleMuestra.this));
        recyclerView.setNestedScrollingEnabled(false);





        Bundle bundle = getIntent().getExtras();

        codigoMuestra.setText(bundle.getString("codigo"));
        tipoAnimal.setText(bundle.getString("animal"));
        fechaIngreso.setText(bundle.getString("fecha"));
        fechaVencimiento.setText(bundle.getString("fechaV"));
        estadoMuestra.setText(bundle.getString("estado"));
        if (estadoMuestra.getText().equals("en espera")) {
            estadoMuestra.setBackgroundResource(R.drawable.background_estado_espera);
            estadoMuestra.setText("En espera");
        } else if (estadoMuestra.getText().equals("en analisis")) {
            estadoMuestra.setText("En análisis");
            estadoMuestra.setBackgroundResource(R.drawable.background_estado_en_analisis);
        } else if (estadoMuestra.getText().equals("finalizada")) {
            estadoMuestra.setText("Finalizada");
            estadoMuestra.setBackgroundResource(R.drawable.background_estado_finalizada);
        }
        jsonRequestAnalisis(bundle.getString("codigo"));



    }

    private void jsonRequestAnalisis(String codigo) {
        String JSON_URL = ip + endpointObtenerAnalisis + codigo;
        Log.d("URL", JSON_URL);
        RequestQueue requestQueue = Volley.newRequestQueue(DetalleMuestra.this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Do something with response
                        //mTextView.setText(response.toString());
                        Log.d("response", response.toString());
                        // Process the JSON
                        try {
                            // Get the JSON array
                            JSONArray array = response.getJSONArray("analisis");
                            // Loop through the array elements
                            for (int i = 0; i < array.length(); i++) {
                                // Get current json object
                                JSONObject analisis = array.getJSONObject(i);
                                Log.d("Respuesta", analisis.toString());
                                arrayList.add(new FeedData(analisis.getString("nombre"), analisis.getString("codigo"),analisis.getString("estado_analisis")));

                                // Display the formatted json data in text view

                                Log.d("PasoAnalisis", "si paso por aqui");
                            }
                            setAdapter();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            Log.d("ERROR", e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        recyLa.removeView(recyclerView);
                        View errorCard = LayoutInflater.from(DetalleMuestra.this).inflate(R.layout.card_error,null);
                        TextView textoError = errorCard.findViewById(R.id.errorText);
                        textoError.setText("Error: "+error.getMessage());
                        recyLa.addView(errorCard);
                    }
                }

        );

        // Add JsonObjectRequest to the RequestQueue
        requestQueue.add(jsonObjectRequest);
    }


    private void setAdapter() {
        Log.e("setAdapter","setAdapter: " + arrayList.size());
        adapter = new FeedAdapter(arrayList,new FeedAdapter.MyAdapterListener() {
            @Override
            public void buttonViewOnClick(View v, int position) {
                jsonRequestDetalleAnalisis(String.valueOf(v.getTag()));
            }

            });
        recyclerView.setAdapter(adapter);
    }

    private void jsonRequestDetalleAnalisis(String codigoAnalisis) {
        String JSON_URL = ip+endpointObtenerDetalleAnalisis+codigoAnalisis;
        Log.d("URL",JSON_URL);
        RequestQueue requestQueue = Volley.newRequestQueue(DetalleMuestra.this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Do something with response
                        //mTextView.setText(response.toString());
                        Log.d("response", response.toString());
                        // Process the JSON
                        try{
                            // Get the JSON array
                            JSONArray array = response.getJSONArray("analisis");
                            // Loop through the array elements
                            for(int i=0;i<array.length();i++){
                                // Get current json object
                                JSONObject analisis = array.getJSONObject(i);
                                Log.d("Respuesta", analisis.toString());

                                // Get the current student (json object) data

                                // Display the formatted json data in text view

                                Log.d("Paso","si paso por aqui");

                                String codigoAnalisis = analisis.getString("codigo");
                                String nombreAnalisis = analisis.getString("nombre");
                                String fechaIngreso = analisis.getString("fecha_inscripcion");
                                String estado = analisis.getString("estado_analisis");


                                Intent intent = new Intent(DetalleMuestra.this, DetalleAnalisis.class);
                                intent.putExtra("codigo",codigoAnalisis);
                                intent.putExtra("nombre", nombreAnalisis);
                                intent.putExtra("fecha", fechaIngreso);
                                intent.putExtra("estado",estado);


                                startActivity(intent);

                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            Log.d("ERROR", e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", error.toString());
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

        );

        // Add JsonObjectRequest to the RequestQueue
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    protected void onResume(){
        super.onResume();


    }

}




