package com.udec.SISGELAB;

import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetalleAnalisis extends AppCompatActivity {

    TextView codigoAnalisis;
    TextView nombreAnalisis;
    TextView fechaIngreso;
    TextView estado;
    ViewGroup recyLa;
    String ip="http://192.168.43.113";
    String endPointObtenerSubanalisis="/api/subanalisis/obtener-subanalisis-analisis?codigo=";

    RecyclerView recyclerView;
    AdapterFeedSubanalisis adapter;
    AdapterFeedErrorSubanalisis adapterError;

    ArrayList<AdapterDataSubanalisis> arrayList = new ArrayList<>();
    ArrayList<AdapterDataErrorSubanalisis> arrayListError = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_analisis);
        codigoAnalisis = findViewById(R.id.codigoAnalisis);
        nombreAnalisis = findViewById(R.id.tipoAnalisis);
        fechaIngreso = findViewById(R.id.fechaIngresoAnalisis);
        estado = findViewById(R.id.estadoAnalisisText);
        recyclerView = findViewById(R.id.listaSubanalisis);
        recyclerView.setLayoutManager(new LinearLayoutManager(DetalleAnalisis.this));


        Bundle bundle = getIntent().getExtras();
        codigoAnalisis.setText(bundle.getString("codigo"));
        nombreAnalisis.setText(bundle.getString("nombre"));
        fechaIngreso.setText(bundle.getString("fecha"));
        estado.setText(bundle.getString("estado"));

        if(estado.getText().equals("en proceso")){
            estado.setText("En proceso");
            estado.setBackgroundResource(R.drawable.background_estado_analisis_proceso);


        }else if(estado.getText().equals("no iniciado")) {
            estado.setText("No iniciado");
            estado.setBackgroundResource(R.drawable.background_estado_analisis_no_iniciado);

        }else if(estado.getText().equals("en revision")){
            estado.setText("En revisión");
            estado.setBackgroundResource(R.drawable.background_estado_analisis_en_revision);

        }else if(estado.getText().equals("finalizado")){
            estado.setText("Finalizado");
            estado.setBackgroundResource(R.drawable.background_estado_analisis_finalizado);


        }else if(estado.getText().equals("vencido")){
            estado.setText("Vencido");
            estado.setBackgroundResource(R.drawable.background_estado_analisis_en_vencido);


        }

        jsonRequestAnalisis(bundle.getString("codigo"));
    }

    private void jsonRequestAnalisis(String codigo) {
        String JSON_URL = ip+endPointObtenerSubanalisis + codigo;
        Log.d("URL", JSON_URL);
        RequestQueue requestQueue = Volley.newRequestQueue(DetalleAnalisis.this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Do something with response
                        //mTextView.setText(response.toString());
                        Log.d("response", response.toString());
                        // Process the JSON
                        try {
                            // Get the JSON array
                            JSONArray array = response.getJSONArray("subanalisis");
                            // Loop through the array elements
                            if(array.length() == 0) {
                                arrayListError.add(new AdapterDataErrorSubanalisis("No se han iniciado análisis a esta muestra"));
                                setAdapterError();
                            }else {
                                for (int i = 0; i < array.length(); i++) {
                                    // Get current json object
                                    JSONObject subanalisis = array.getJSONObject(i);
                                    Log.d("Respuesta", subanalisis.toString());
                                    arrayList.add(new AdapterDataSubanalisis(subanalisis.getString("codigo_subanalisis"), subanalisis.getString("codigo_submuestra"),subanalisis.getString("tipo_submuestra"),subanalisis.getString("nombre_quimico"),subanalisis.getString("estado_subanalisis")));

                                    // Display the formatted json data in text view

                                    Log.d("PasoAnalisis", "si paso por aqui");
                                }
                                setAdapter();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            Log.d("ERROR", e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        arrayListError.add(new AdapterDataErrorSubanalisis(error.getMessage()));

                    }
                }

        );
        if(jsonObjectRequest==null){

        }else{

        }

        // Add JsonObjectRequest to the RequestQueue
        requestQueue.add(jsonObjectRequest);
    }

    private void setAdapter() {
        Log.e("setAdapter", "setAdapter: " + arrayList.size());
        adapter = new AdapterFeedSubanalisis(arrayList);
        recyclerView.setAdapter(adapter);
    }
    private void setAdapterError() {
        Log.e("setAdapter", "setAdapter: " + arrayListError.size());
        adapterError = new AdapterFeedErrorSubanalisis(arrayListError);
        recyclerView.setAdapter(adapterError);
    }
}
