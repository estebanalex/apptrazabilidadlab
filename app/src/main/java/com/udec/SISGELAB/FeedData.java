package com.udec.SISGELAB;

public class FeedData {
    private String codigoAnalisis;
    private String estadoAnalisis;
    private String nombreAnalisis;

    public FeedData(String nombreAnalisis, String codigoAnalisis, String estadoAnalisis) {
        this.codigoAnalisis = codigoAnalisis;
        this.estadoAnalisis = estadoAnalisis;
        this.nombreAnalisis = nombreAnalisis;
    }

    public String getCodigoAnalisis() {
        return codigoAnalisis;
    }

    public void setCodigoAnalisis(String codigoAnalisis) {
        this.codigoAnalisis = codigoAnalisis;
    }

    public String getEstadoAnalisis() {
        return estadoAnalisis;
    }

    public void setEstadoAnalisis(String estadoAnalisis) {
        this.estadoAnalisis = estadoAnalisis;
    }

    public String getNombreAnalisis() {
        return nombreAnalisis;
    }

    public void setNombreAnalisis(String nombreAnalisis) {
        this.nombreAnalisis = nombreAnalisis;
    }
}
