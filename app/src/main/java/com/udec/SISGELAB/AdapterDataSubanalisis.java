package com.udec.SISGELAB;

public class AdapterDataSubanalisis {
    private String codigoSubnalisis;
    private String codigoSubmuestra;
    private String nombreSubmuestra;
    private String responsable;
    private String estado;

    public AdapterDataSubanalisis(String codigoSubnalisis, String codigoSubmuestra, String nombreSubmuestra, String responsable, String estado) {
        this.codigoSubnalisis = codigoSubnalisis;
        this.codigoSubmuestra = codigoSubmuestra;
        this.nombreSubmuestra = nombreSubmuestra;
        this.responsable = responsable;
        this.estado = estado;
    }

    public String getCodigoSubnalisis() {
        return codigoSubnalisis;
    }

    public void setCodigoSubnalisis(String codigoSubnalisis) {
        this.codigoSubnalisis = codigoSubnalisis;
    }

    public String getCodigoSubmuestra() {
        return codigoSubmuestra;
    }

    public void setCodigoSubmuestra(String codigoSubmuestra) {
        this.codigoSubmuestra = codigoSubmuestra;
    }

    public String getNombreSubmuestra() {
        return nombreSubmuestra;
    }

    public void setNombreSubmuestra(String nombreSubmuestra) {
        this.nombreSubmuestra = nombreSubmuestra;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
